FROM nginx:stable

RUN apt-get update && apt-get upgrade -y;
RUN apt-get install certbot python-certbot-nginx -y;

COPY add-hostnames.sh /usr/local/bin/
COPY config.sh /usr/local/bin/
COPY config/nginx/default.conf.dist /etc/nginx/conf.d/default.conf.dist
COPY config/nginx/server_name.conf.inc.dist /etc/nginx/conf.d/server_name.conf.inc.dist

RUN ln -s /usr/local/bin/config.sh; \
    ln -s /usr/local/bin/add-hostnames.sh; \
    chmod +x /usr/local/bin/config.sh; \
    chmod +x /usr/local/bin/add-hostnames.sh;
