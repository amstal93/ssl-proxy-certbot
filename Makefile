install: destroy build run-config

destroy:
	@docker stop nginx-proxy > /dev/null 2>&1 || true; \
	docker rm nginx-proxy > /dev/null 2>&1 || true; \

build:
	@docker build -q -t palliis/ssl-proxy-certbot:latest . > /dev/null 2>&1; \

run-config:
	@docker run -it \
	--name nginx-proxy \
	--publish 80:80 \
	--publish 443:443 \
	--volume /etc/nginx/certs \
	--volume /etc/nginx/conf.d \
	--volume /etc/letsencrypt \
	palliis/ssl-proxy-certbot:latest config.sh; \
	docker start `docker ps -q -l`; \
	docker exec nginx-proxy service nginx start;

add-hostnames:
	@docker exec -it nginx-proxy add-hostnames.sh; \
	docker exec nginx-proxy service nginx restart;
