# SSL Proxy Certbot

HTTPS proxy with SSL certificates from Let's Encrypt

## Resources
[Docker Hub](https://hub.docker.com/r/palliis/ssl-proxy-certbot)

[Gitlab](https://gitlab.com/palliis/ssl-proxy-certbot)

## Motivation
When working with domain names owned by another party, setting up SSL 
certificates can be a problem. Most cloud solutions offer a solution for
requesting SSL Certificates from their Load Balancers, but they require
you to add extra DNS records for verification (DNS-01 challenge) which
is prone to human mistakes and often requires manual work.

Furthermore, after a cloud solution is setup correctly, you have created
a vendor lock with said cloud provider. Migrating away to another 
service can introduce a lot of work, since every domain you had setup
before needs new DNS records for verification. If your application is 
used by multiple third parties at the same time, you are required to 
wait for every party to update their DNS before you are able to migrate
your application.

SSL Proxy Certbot tries to resolve this by providing a service that uses
Certbot for HTTP-01 challenges. This allows you to setup third party
domain names once to simply point to the IP address of the service this 
application is running on. 

No complicated DNS records needed. They are only required to setup their
domain with A + AAAA records pointing to your server. Or even better, 
add a single CNAME you have setup pointing to your server. 


## Requirements
General use:
- [Docker](https://docs.docker.com/install/)

Development:
- [GNU Make](https://www.gnu.org/software/make/)

## How to use

Start the initial setup by running:
```console
$ docker run -it \
	--name nginx-proxy \
	--publish 80:80 \
	--publish 443:443 \
	--volume /etc/nginx/certs \
	--volume /etc/nginx/conf.d \
	--volume /etc/letsencrypt \
	palliis/ssl-proxy-certbot:latest config.sh; \
	docker start `docker ps -q -l`; \
	docker exec nginx-proxy service nginx start;
```

The installation wizard will ask you to choose a proxy target to point your HTTPS request to after the certificates are installed. This should be a full hostname, i.e. `foobar.example.com`.
```console
Enter a proxy target hostname to forward HTTPS requests to: foobar.example.com
```
 
After the proxy target is configured, the wizard asks if you want to add hostname. 
Add all hostnames you want to request SSL certificates for. Again, this should be a full hostname, i.e. `acme.example.com`.
```console
Add another hostname to request a certificate for? [y/n]: y
Enter a hostname to add: acme.example.com
Config updated. Now contains: 
server_name
    acme.example.com
;
```

Repeat this process for all hostnames you would like to request a certificate for.
Note that all the hostnames you add here need to point their DNS to the server you are running the docker container on. 

When you are done adding all hostnames, enter `n` in the prompt.
```console
Add another hostname to request a certificate for? [y/n]: n
```

Now the Certbot configuration will start. 

The Certbot configuration has a number of prompts that asks you to enter your email address and to accept their terms of service. 

These steps are mandatory. 

Another prompt asks you if you are willing to share your email address with the Electronic Frontier Foundation. This is optional. 

When you see the following prompt, verify that all your hostnames are correct and press ENTER.
Again, make sure that all the hostnames you see here point their DNS to the server you are running the docker container on.
```console
Which names would you like to activate HTTPS for?
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
1: acme.example.com
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
Select the appropriate numbers separated by commas and/or spaces, or leave input
blank to select all options shown (Enter 'c' to cancel): 
```

When the following prompt comes up, make sure you enter option `2`:
```console
Please choose whether or not to redirect HTTP traffic to HTTPS, removing HTTP access.
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
1: No redirect - Make no further changes to the webserver configuration.
2: Redirect - Make all requests redirect to secure HTTPS access. Choose this for
new sites, or if you're confident your site works on HTTPS. You can undo this
change by editing your web server's configuration.
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
Select the appropriate number [1-2] then [enter] (press 'c' to cancel): 2
```

After the initial setup is complete, you should be able to go to your configured hostnames, i.e. `acme.example.com` and see the contents of `foobar.example.com`, but with a HTTPS connection using a valid Let's Encrypt certificate. 

If you want to add hostnames after the initial setup, run:
```console
$ docker exec -it nginx-proxy add-hostnames.sh; \
	docker exec nginx-proxy service nginx restart;
``` 

## Licence
[MIT License](https://gitlab.com/palliis/ssl-proxy-certbot/blob/master/LICENSE)

## Credits

[Electronic Frontier Foundation](https://www.eff.org/)

[Let's Encrypt](https://letsencrypt.org/)

[NGINX](https://www.nginx.com/)

[Docker](https://www.docker.com/)
