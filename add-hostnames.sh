#!/usr/bin/env bash

read -r -p "Add another hostname to request a certificate for? [y/n]: " ANSWER_YES_NO; \
case ${ANSWER_YES_NO} in
    [Yy]* ) \
        read -r -p "Enter a hostname to add: " SERVER_NAME; \
        touch /tmp/temp.txt && \
        head -n -1 /etc/nginx/conf.d/server_name.conf.inc > /etc/nginx/conf.d/server_name.temp; \
        mv /etc/nginx/conf.d/server_name.temp /etc/nginx/conf.d/server_name.conf.inc; \
        echo "    $SERVER_NAME" >> /etc/nginx/conf.d/server_name.conf.inc; \
        echo ';' >> /etc/nginx/conf.d/server_name.conf.inc; \
        echo 'Config updated. Now contains: '; \
        cat /etc/nginx/conf.d/server_name.conf.inc; \
        sh add-hostnames.sh;
esac
