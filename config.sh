#!/usr/bin/env bash

if test ! -f "/etc/nginx/conf.d/server_name.conf.inc"; then
    echo "Detected initial startup. Configuring NGINX...";
    rm /etc/nginx/conf.d/default.conf;
    cp /etc/nginx/conf.d/default.conf.dist /etc/nginx/conf.d/default.conf;
    read -r -p "Enter a proxy target hostname to forward HTTPS requests to: " PROXY_TARGET;
    sed -i "s/someserver.example.com/$PROXY_TARGET/g" /etc/nginx/conf.d/default.conf;
    mv /etc/nginx/conf.d/server_name.conf.inc.dist /etc/nginx/conf.d/server_name.conf.inc;
    echo "Completed configuration of NGINX, initializing Certbot...";
    sh add-hostnames.sh;
    certbot --nginx;
    echo "Finished setup. All done.";
else
    sh add-hostnames.sh;
    certbot --nginx;
fi;
